﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ExamplePlayerController : MonoBehaviour
{
	public Color materialColor;
	public Rigidbody m_rigidbody;

	public string horizontalAxis = "Horizontal";
	public string verticalAxis = "Vertical";
	public string jumpButton = "Jump";

	private float inputHorizontal;
	private float inputVertical;

	void Awake()
	{
		GetComponent<Renderer>().material.color = materialColor;
		m_rigidbody = GetComponent<Rigidbody>();
	}

	void Update()
	{
		if (m_rigidbody.position.y < -5)
		  Die();

		inputHorizontal = SimpleInput.GetAxis(horizontalAxis);
		inputVertical = SimpleInput.GetAxis(verticalAxis);

		GetComponent<Rigidbody>().velocity = new Vector3(inputHorizontal * 10, m_rigidbody.velocity.y, inputVertical * 10);
	}

	void OnCollisionEnter(Collision collision) {
      if (collision.gameObject.tag == "Deadly")
		  Die();

      if (collision.gameObject.tag == "Winning"){
		  SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
	  }
    }

	void Die() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}
}